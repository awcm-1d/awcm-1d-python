# THIS SCRIPT TESTS THE DERIVATIVES USING WAVELET TRANSFORM WFT ON AN INTERVAL
# Written In Matlab:    Oleg V. Vasilyev    |   14 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   30 November, 2023

import os
import copy 
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
from context import (
    wtf,
    significant,
    adjust_lev,
    add_adjacent,
    reconstruction_check,
    add_ghost,
    calc_derivative_mask,
    DUMMY_FLOAT
    )


Type        = 'interval'                         #Domain type: 'interval' or 'periodic'
cl          = 2**3                               #RESOLUTION ON COARSEST LEVEL
jlev        = 4                                  #NUMBER OF LEVELS
Jmx         = 7                                  #MAXIMUM NUMBER OF LEVELS ALLOWED
ende        = 1 + cl * 2**(jlev-1)               #RESOLUTION ON FINEST LEVEL FOR NON-PERIODIC CASE
order       = 3
der_order   = 3
acc         = 0.001                              #ACCURACY CONTROL OVER WAVELET AMPLITUDES

# Post Processing Parameters
base_filename = "test_calc_derivative"
title_size = 8
tick_size  = 8
label_size = 8




if Type.lower() == 'periodic':
    ende    = ende - 1

L    = np.ones(ende+1, dtype=bool)
L[0] = False

#SPECIFY SIGNAL
a      = [DUMMY_FLOAT, 1.0,  1.0]
b      = [DUMMY_FLOAT, 0.25, 0.25]
c      = [DUMMY_FLOAT, 1.e5, 0.02]

#SPECIFY SIGNAL
h      = 1.0/(ende - np.mod(ende, 2))                      #h - mesh spacing on the finest level
X      = np.full(ende+1, DUMMY_FLOAT, dtype=np.float64)    
X[1:]  = h * np.arange(0, ende, 1)                         #X - grid on finest level

F      = np.zeros(np.size(X))
dFa    = np.zeros(np.size(X))
d2Fa   = np.zeros(np.size(X))

for k in np.arange(1, len(a)):
    F    = F    + a[k]*np.exp(-(X-b[k])**2/c[k]**2)
    dFa  = dFa  - a[k]*np.exp(-(X-b[k])**2/c[k]**2)*(2.0*(X-b[k])/c[k]**2)
    d2Fa = d2Fa + a[k]*np.exp(-(X-b[k])**2/c[k]**2)*(4.0*(X-b[k])**2/c[k]**4 - 2.0/c[k]**2)
#     F    = F    +          a[k]*sin(2*pi*(X-b[k]))
#     dFa  = dFa  + 2.*pi   *a[k]*cos(2*pi*(X-b[k]))
#     d2Fa = d2Fa - 4.*pi**2*a[k]*sin(2*pi*(X-b[k]))


#ADAPT GRID================================
scl       = max(F[L])-min(F[L])

Fn        = wtf(Type, F,  jlev, order, 'forward', L)

L_sig     = significant(Fn, L, scl, acc, jlev)

[Fn, L_sig, jlev, ende] = adjust_lev(Fn, L_sig, jlev, Jmx)

L_sig     = add_adjacent(Type, L_sig, jlev)

L         = reconstruction_check(Type, jlev, order, L_sig)

[G, j_df] = add_ghost(L, Type, jlev, order)
#=========================================

#SPECIFY SIGNAL on the new mesh
h      = 1.0/(ende - np.mod(ende, 2))                      #h - mesh spacing on the finest level
X      = np.full(ende+1, DUMMY_FLOAT, dtype=np.float64)    
X[1:]  = h * np.arange(0, ende, 1)                         #X - grid on finest level
F      = np.zeros(np.size(X))
dFa    = np.zeros(np.size(X))
d2Fa   = np.zeros(np.size(X))

k_range = np.arange(1, len(a))
for k in k_range:
    F    = F    + a[k]*np.exp(-(X-b[k])**2/c[k]**2)
    dFa  = dFa  - a[k]*np.exp(-(X-b[k])**2/c[k]**2)*(2.0*(X-b[k])/c[k]**2)
    d2Fa = d2Fa + a[k]*np.exp(-(X-b[k])**2/c[k]**2)*(4.0*(X-b[k])**2/c[k]**4 - 2.0/c[k]**2)
#     F    = F    +          a[k]*sin(2*pi*(X-b[k]))
#     dFa  = dFa  + 2.*pi   *a[k]*cos(2*pi*(X-b[k]))
#     d2Fa = d2Fa - 4.*pi**2*a[k]*sin(2*pi*(X-b[k]))
#==============================

Fn     = wtf(Type, Fn, jlev, order, 'inverse', L)
Fn[L]  = copy.deepcopy(F[L])
Fn[~L] = np.NaN

[dF, d2F] = calc_derivative_mask(Fn, L, G, j_df, Type, jlev, order, h, der_order)
#[dF, d2F] = calc_derivative(Fn, L, Type, jlev, order, h, der_order)


fig_1, axes = plt.subplots(nrows=3, ncols=1)
fig_1.tight_layout()

#VISUALIZE SIGNAL =================================================================================================================
plt.subplot(3,1,1)
plt.plot(X[1:], dFa[1:]  / max(abs(dFa[1:])),  'k-')
plt.plot(X[L],  dF[L]    / max(abs(dFa[1:])),  'rx')
plt.plot(X[1:], d2Fa[1:] / max(abs(d2Fa[1:])), 'k-')
plt.plot(X[L],  d2F[L]   / max(abs(d2Fa[1:])), 'bx')
plt.title('Normalized Derirvative:  Compression: ' + str(format(1-sum(L[1:])/ende,".6f")))

#VISUALIZE TREE ===================================================================================================================
plt.subplot(3,1,2)
Level   = jlev * np.ones(np.size(dF))

j_range = np.arange(jlev-1, 0, -1, dtype=int)
for j in j_range:
    s = 2**(jlev-j-1)
    Level[1:ende+1:2*s] = j

plt.plot(X[1:], Level[1:], 'kx')
plt.plot(X[G],  Level[G],  'bo')
plt.plot(X[L],  Level[L],  'rx')
plt.title('Original jlev: ' + str(jlev) + '  |  New jlev: ' + str(jlev))

plt.subplot(3,1,3)
plt.semilogy(X[L], abs(dFa[L]-dF[L])/max(abs(dFa[1:])), '-r')
plt.title('Normalized Derivative Error' +
         '  |  $\mathregular{Error_{D}}$='  + str(format(max(abs(dFa[L]-dF[L])),".6f")) + 
         '  |  $\mathregular{Error_{D2}}$=' + str(format(max(abs(d2Fa[L]-d2F[L])),".6f")) )
plt.semilogy(X[L], abs(d2Fa[L]-d2F[L])/max(abs(d2Fa[1:])), '-b')



for ax in [0,1,2]:
    axes[ax].xaxis.set_tick_params(labelsize=tick_size)
    axes[ax].yaxis.set_tick_params(labelsize=tick_size)
    axes[ax].xaxis.label.set_size(label_size)
    axes[ax].yaxis.label.set_size(label_size)
    axes[ax].title.set_size(title_size)
    axes[ax].title.set_weight("bold")

for ax in [0,2]:
    axes[ax].spines['top'].set_visible(False)
    axes[ax].spines['right'].set_visible(False)

axes[1].axes.get_yaxis().set_ticks(np.arange(1, jlev+1, 1, dtype=int))
#axes[1].axes.get_xaxis().set_ticks([])

axes[1].axes.set_xlim()
axes[1].axes.set_ylim([0, jlev+1])
# set(gca, 'YTick', [1:jlev]);
# axis([0, 1, 0, jlev+1]);

timestamp = datetime.now()
timestamp = timestamp.strftime('%Y%m%dT%H%M%S%f')
run_directory_base = timestamp
os.mkdir(run_directory_base)
fig_1.savefig(run_directory_base+'/jlev='+str(jlev)+'__'+base_filename+'.pdf')
fig_1.savefig(run_directory_base+'/jlev='+str(jlev)+'__'+base_filename+'.jpg', dpi=300)
plt.clf()
plt.cla()
plt.close('all')
del fig_1
del axes