# THIS SCRIPT TESTS THE WAVELET TRANSFORM WFT ON AN INTERVAL
# Written In Matlab:    Oleg V. Vasilyev    |   14 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   29 November, 2023

import os
import copy 
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
from context import (
    wtf,
    significant,
    adjust_lev,
    add_adjacent,
    reconstruction_check,
    add_ghost,
    DUMMY_FLOAT
    )


cl          = 2**3                               #RESOLUTION ON COARSEST LEVEL
jlev        = 3                                  #NUMBER OF LEVELS
jlev_ori    = copy.deepcopy(jlev)
Jmx         = 7                                  #MAXIMUM NUMBER OF LEVELS ALLOWED
ende        = 1 + cl * 2**(jlev-1)               #RESOLUTION ON FINEST LEVEL - THIS IS UNIQUE POINTS
order       = 3
acc         = 0.001                              #ACCURACY CONTROL OVER WAVELET AMPLITUDES
Type        = 'periodic'

# Post Processing Parameters
base_filename = "test_adapt_grid"
title_size = 8
tick_size  = 8
label_size = 8

timestamp = datetime.now()
timestamp = timestamp.strftime('%Y%m%dT%H%M%S%f')
run_directory_base = timestamp
os.mkdir(run_directory_base)




if Type.lower() == 'periodic':
    ende = ende - 1

L    = np.ones(ende+1, dtype=bool) #Matlab logical(ones(1,ende));
L[0] = False

#SPECIFY SIGNAL
a = [DUMMY_FLOAT, 1.0, 1.0]
b = [DUMMY_FLOAT, 0.5, 0.5]
c = [DUMMY_FLOAT, 1.e5, 0.02]

X_vec            = np.full(ende+1, DUMMY_FLOAT, dtype=np.float64)
X_vec[1:ende+1]  = np.linspace(0, 1.0, num=ende, endpoint=True, dtype=np.float64)

F_a = np.zeros(np.size(X_vec))

for k in np.arange(1, len(a)):
    F_a = F_a + a[k] * np.exp(-(X_vec-b[k])**2. / c[k]**2.)


for iter in np.arange(1, Jmx-jlev+1, 1, dtype=int):

    Fn = copy.deepcopy(F_a)
    Fn[~L] = np.NaN
    scl = max(Fn[L][1:])-min(Fn[L][1:])
    
    Wtf = wtf(Type, Fn, jlev, order, 'forward', L)

    L_sig = significant(Wtf, L, scl, acc, jlev)

    Wtf, L_sig, jlev, ende = adjust_lev(Wtf, L_sig, jlev, Jmx)

    L_sig = add_adjacent(Type, L_sig, jlev)

    L = reconstruction_check(Type, jlev, order, L_sig)

    G, j_df = add_ghost(L, Type, jlev, order)
        
    #DO TWO DIFFERENT RESTORES - ONE TO ALL POINTS - ONE TO ONLY FLAGGED POINTS
    #X_vec: X-grid on finest level
    X_vec = np.full(ende+1, DUMMY_FLOAT, dtype=np.float64)
    X_vec[1:ende+1] = np.linspace(0, 1.0, num=ende, endpoint=True, dtype=np.float64)

    F_a = np.zeros(np.size(X_vec)) 
    for k in np.arange(1, len(a)):
        F_a = F_a + a[k] * np.exp(-(X_vec-b[k])**2. / c[k]**2.)  #Resetting analytical function

    fig_1, axes = plt.subplots(nrows=3, ncols=1)
    fig_1.tight_layout()

    for i in [1,2]:
        if i==1:
            #DO COMPLETE RESTORE  ===============================================================================================
            Wtf[~L] = 0.
            L_all = np.ones(np.size(L), dtype=bool)  #logical(ones(size(L)))
            L_all[0] = False
            Wtf_inv = wtf(Type, Wtf, jlev, order, 'Inverse', L_all)
        else:
            #DO RESTORE ONLY ON FLAGGED POINTS
            Wtf[~L] = np.NaN
            del Fn
            Fn = wtf(Type, Wtf, jlev, order, 'Inverse', L)

        #VISUALIZE SIGNAL ===================================================================================================================
        plt.subplot(3, 1, 1)
        if i==1:
            plt.plot(X_vec[1:], F_a[1:], 'k-')
            plt.title('Acc: '+str(acc)+'  |  Error: '+str(format(max(abs(Wtf_inv[1:]-F_a[1:])),".5f"))+'  |  Sav: '+str(format(1-sum(L[1:])/ende,".5f")))
        else:
            plt.plot(X_vec[L], Fn[L], 'rx-')
        
        #VISUALIZE TREE ===================================================================================================================
        plt.subplot(3, 1, 2)
        Level = jlev*np.ones(np.size(X_vec))
        j_range = np.arange(jlev-1, 0, -1, dtype=int) #jlev-1:-1:1
        for j in j_range:
            s                   = 2**(jlev-j-1)
            Level[1:ende+1:2*s] = j
        
        if i==1:
            plt.plot(X_vec[1:],    Level[1:],    'kx')
            plt.title('Original jlev: '+str(jlev_ori)+'  |  New jlev: '+str(jlev))
        else:
            plt.plot(X_vec[L], Level[L], 'rx')
        

        plt.subplot(3, 1, 3)
        if i==1:
            plt.plot(X_vec[1:], abs(F_a[1:]-Wtf_inv[1:]),'-k')
            plt.title('Error of the original and reconstructed function')
        else:
            plt.plot(X_vec[L], abs(F_a[L]-Fn[L]),'xr')


    for ax in [0,1,2]:
        axes[ax].xaxis.set_tick_params(labelsize=tick_size)
        axes[ax].yaxis.set_tick_params(labelsize=tick_size)
        axes[ax].xaxis.label.set_size(label_size)
        axes[ax].yaxis.label.set_size(label_size)
        axes[ax].title.set_size(title_size)
        axes[ax].title.set_weight("bold")

    for ax in [0,2]:
        axes[ax].spines['top'].set_visible(False)
        axes[ax].spines['right'].set_visible(False)

    axes[1].axes.get_xaxis().set_ticks([])
    axes[1].axes.get_yaxis().set_ticks([])

    # os.mkdir(run_directory_base+'/jlev='+str(jlev))
    fig_1.savefig(run_directory_base+'/jlev='+str(jlev)+'__'+base_filename+'.pdf')
    fig_1.savefig(run_directory_base+'/jlev='+str(jlev)+'__'+base_filename+'.jpg', dpi=300)
    plt.clf()
    plt.cla()
    plt.close('all')
    del fig_1
    del axes
