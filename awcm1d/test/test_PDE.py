# THIS SCRIPT TESTS THE DERIVATIVES USING WAVELET TRANSFORM WFT ON AN INTERVAL
# Written In Matlab:    Oleg V. Vasilyev    |   14 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   6 January, 2024

import os
import copy 
import numpy as np
import scipy
from matplotlib import pyplot as plt
from datetime import datetime
from context import (
    adapt_grid,
    diff_wlt,
    DUMMY_FLOAT
    )




#Initial Conditions
def IC(x):    
    #f = zeros(size(x))
    f = np.sin(2.0 * np.pi * x)
    return f


#Boundary Conditions
def BC(flag):

    if flag.lower() == 'rhs':
        y_l = 0.0
        y_r = 0.0
    elif flag.lower() == 'bc':
        y_l = 0.0
        y_r = 0.0

    return y_l, y_r


#Evolution Equation
def rhs(t, y, nu, DomainType, L, jlev, order, h, der_order, XX, Jmx):
    x, dy, d2y = diff_wlt(y, L, DomainType, jlev, order, h, der_order, XX, Jmx)
    dydt = ( -y*dy + nu*d2y )   # + 32*(0.25-x).*exp(-64*(x-0.25).^2)
   
    #Boundary Conditions
    if DomainType.lower() == 'interval':
        dydt[1], dydt[-1] = BC('rhs')
    
    return dydt



#===========================================================

DomainType     = 'periodic'                      #Domain type: 'interval' or 'periodic'
cl          = 2**2                               #RESOLUTION ON COARSEST LEVEL
jlev        = 4                                  #NUMBER OF LEVELS
Jmx         = 8                                  #MAXIMUM NUMBER OF LEVELS ALLOWED
ende        = 1 + cl * 2**(jlev-1)               #RESOLUTION ON FINEST LEVEL FOR NON-PERIODIC CASE
ende_mx     = 1 + cl * 2**(Jmx-1)                #RESOLUTION ON FINEST LEVEL FOR NON-PERIODIC CASE
order       = 3
der_order   = 3
acc         = 0.01                               #ACCURACY CONTROL OVER WAVELET AMPLITUDES
CFL         = 0.1                                # CFL
Tend        = 1.0                                # time interval
dt          = 1.e-03                             # initial time step
nu          = 0.01                               # viscosity
scl_floor   = 0.1                                # scl = max(scl, scl_floor)

# Post Processing Parameters
base_filename = "test_PDE"
title_size = 8
tick_size  = 8
label_size = 8



if DomainType.lower() == 'periodic':
    ende    = ende - 1
    ende_mx = ende_mx - 1     

L      = np.ones(ende+1, dtype=bool)
L[0]   = False

hh     = 1.0/(ende_mx - np.mod(ende_mx,2))                     #hh - mesh spacing on the finest level
XX     = np.full(ende_mx+1, DUMMY_FLOAT, dtype=np.float64)    
XX[1:] = hh * np.arange(0, ende_mx, 1)                         #XX - grid on finest level

#SPECIFY SIGNAL
s      = 2**(Jmx-jlev)
h      = hh*s                                                  #h - mesh spacing on the finest level
X      = np.full(int(ende_mx/s)+1, DUMMY_FLOAT, dtype=np.float64)    
X[1:]  = XX[1::s]                                              #X - grid on finest level

#INITIAL CONDITIONS
for iter in np.arange(1, Jmx-jlev+2, 1, dtype=int):
    Fn     = IC(X)
    Fn[~L] = np.NaN
    scl    = max(0.5*(max(Fn[L])-min(Fn[L])), scl_floor)
    
    #ADAPT GRID
    Fn, L, G, j_df, X, s, h, jlev, ende = adapt_grid(Fn, L, DomainType, jlev, Jmx, scl, acc, order, hh, XX)
    
    Fn[L]  = IC(X[L])
    Fn[~L] = np.NaN


#Post-Processing
timestamp = datetime.now()
timestamp = timestamp.strftime('%Y%m%dT%H%M%S%f')
run_directory_base = timestamp
os.mkdir(run_directory_base)


#Time advancement
t = 0.0
# ode15s = scipy.integrate.ode(rhs).set_integrator('vode', method='bdf', order=15)
# ode15s.set_f_params(nu, DomainType, L, jlev, order, h, der_order, XX, Jmx) 
while t < Tend:
   
    dt =  min(1.5*dt, CFL*h/max(Fn[L]))
    
    #options = odeset('RelTol',1e-1*acc,'AbsTol',1e-1*acc,'MaxStep',dt,'InitialStep',dt,'MaxOrder',2)
    #[tout,y] = ode15s(@rhs, [0 dt], Fn(L), options)

    y0     = np.full(np.size(Fn[L])+1, DUMMY_FLOAT, dtype=np.float64)
    y0[1:] = Fn[L]

    # ode15s = scipy.integrate.ode(rhs).set_integrator('vode', method='bdf', order=15)
    # ode15s.set_f_params(nu, DomainType, L, jlev, order, h, der_order, XX, Jmx) 
    # ode15s.set_initial_value(y0, 0.0)
    # ode15s.integrate(ode15s.t + dt)
    # Fn[L]  = copy.deepcopy(ode15s.y[1:]) #L[0] is always False
    # del ode15s
    
    y0    = y0 + dt * rhs(t, y0, nu, DomainType, L, jlev, order, h, der_order, XX, Jmx)  #Euler time integration
    Fn[L] = copy.deepcopy(y0[1:]) #L[0] is always False

    t = t + dt     
    

    #Boundary Conditions
    if DomainType.lower() == 'interval':
        i = find(L)
        Fn[i[1]], Fn[i[-1]] = BC('bc')
    
    #ADAPT GRID
    Fn, L, G, j_df, X, s, h, jlev, ende = adapt_grid(Fn, L, DomainType, jlev, Jmx, scl, acc, order, hh, XX)
    
    #PLOT SOLUTION
    fig_1, axes = plt.subplots(nrows=2, ncols=1)
    fig_1.tight_layout()

    plt.subplot(2,1,1)
    plt.plot(X[L], Fn[L], 'k-')
    plt.plot(X[L], Fn[L], 'rx')
    plt.axis([0, 1, -1, 1])
    axes[1].axes.get_yaxis().set_ticks(np.arange(-1, 1, 1, dtype=int))
    #axes[1].axes.get_xaxis().set_ticks([])
    axes[1].axes.set_xlim()
    axes[1].axes.set_ylim([-1, 1])
    plt.title('Burgers Equation  |  dt=' + str(format(dt,".2e")) + ', t=' + str(format(t,".3f")))
   
    #VISUALIZE TREE ===================================================================================================================
    plt.subplot(2,1,2)
    Level   = jlev * np.ones(np.size(Fn))
    j_range = np.arange(jlev-1, 0, -1, dtype=int) #jlev-1:-1:1
    for j in j_range:
        s = 2**(jlev-j-1)
        Level_ind = np.arange(1, ende+1, 2*s, dtype=int) #[1:2*s:ende];
        Level[Level_ind] = j

    plt.plot(X[G], Level[G], 'bo')
    plt.plot(X[L], Level[L], 'rx')
    axes[1].axes.get_yaxis().set_ticks(np.arange(1, jlev+1, 1, dtype=int))
    axes[1].axes.set_xlim()
    axes[1].axes.set_ylim([0, jlev+1])
    plt.title('Original jlev: ' + str(jlev) + '   New jlev: ' + str(jlev) + '   Using ' + str(100*np.sum(L)/ende) + '% of points')

    #Post-Processing
    fig_1.savefig(run_directory_base+'/t='+str(format(t,".20f"))+'__'+base_filename+'.pdf')
    fig_1.savefig(run_directory_base+'/t='+str(format(t,".20f"))+'__'+base_filename+'.jpg', dpi=300)
    plt.clf()
    plt.cla()
    plt.close('all')
    del fig_1
    del axes