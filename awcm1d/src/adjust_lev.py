# ADJUST_LEV - adjusts level of resolution based on analysis of significant wavelet coefficients
#
# Fn   - is the function or the transformed function
# L_in - mask
# scl  - absolute scale for adaptation
# acc  - relative scale for adaptation
# jlev - number of jleves
# Jmx  - maximum allowable level of resolution
#
# Written In Matlab:    Oleg V. Vasilyev    |   11 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   20 November, 2023

import numpy as np

def adjust_lev(F_in, L_in, jlev_in, Jmx):
    ende = np.size(F_in) - 1
    D_present    = np.zeros(jlev_in+1, dtype=bool) #logical( zeros(jlev_in,1 ));         
    D_present[1] = True                            #Initialize to true because lowest level (C) all are kept.

    j_range = np.arange(1, jlev_in, 1, dtype=int) #1:jlev_in-1
    for j in j_range:
        s     = 2**(jlev_in-j-1)
        D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende]
        
        #CHECK IF ANY WAVELET IS SIGNIFICANT ON THIS LEVEL
        D_present[j+1] = any(L_in[D_ind])

    #ADD ADJACENT WAVELETS
    #  Do not use  np.nonzero(D_present[1:])  as it alters the indices.
    #  Sicne D_present[0] is Flase, it is safe to use np.nonzero(D_present)
    jlev_out = max(jlev_in-1,min(np.max(np.nonzero(D_present))+1,Jmx))  #ADJUST BY ONE LEVEL AT A TIME

    if jlev_out > jlev_in:
        #Stride
        s                   = 2**(jlev_out-jlev_in)             
        
        nprd                = np.mod(ende, 2)
        ende                = s*(ende-nprd) + nprd
        F_out               = np.zeros(ende+1, dtype=np.float64)
        L_out               = np.zeros(ende+1, dtype=bool)
        
        #The C's of the new finest level, the D's remain zero
        C_ind               = np.arange(1, ende+1, s, dtype=int) #1:s:ende;  
        
        #CREATE THE LARGER ARRAYS
        F_out[C_ind]       = F_in[1:]
        L_out[C_ind]       = L_in[1:]
     
    elif jlev_out == jlev_in:
        F_out              = F_in #copy.deepcopy(F_in)
        L_out              = L_in #copy.deepcopy(L_in)

    elif jlev_out < jlev_in:
        #Stride
        s                  = 2**(jlev_in-jlev_out)              

        #The C's of the new finest level
        C_ind              = np.arange(1, ende+1, s, dtype=int) #1:s:ende                        

        #NEW SIZE AND ARRAYS
        nprd               = np.mod(ende, 2)
        ende               = int((ende-nprd)/s) + nprd
        F_out              = np.zeros(ende+1, dtype=np.float64)
        L_out              = np.zeros(ende+1, dtype=bool)
        
        #CREATE THE LARGER ARRAYS
        F_out[1:]          = F_in[C_ind]
        L_out[1:]          = L_in[C_ind]
    
    return F_out, L_out, jlev_out, ende
