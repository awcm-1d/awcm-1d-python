# DIFF_WLT calciulates derivative using global definitions
#
# F         is the function 
#
# Written In Matlab:    Oleg V. Vasilyev    |   4 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   19 November, 2023

import numpy as np
import copy
from awcm1d.src.calc_derivative import calc_derivative as calc_derivative
from awcm1d.src.constants import DUMMY_FLOAT as DUMMY_FLOAT

def diff_wlt(y, L, Type, jlev, order, h, der_order, XX, Jmx):
	X       = np.full(int((np.size(XX)-1)/(2**(Jmx-jlev)))+1, DUMMY_FLOAT, dtype=np.float64)  
	X[1:]   = copy.deepcopy(XX[1::2**(Jmx-jlev)])  #XX(1:2^(Jmx-jlev):end);

	x       = np.full(np.size(y), DUMMY_FLOAT, dtype=np.float64)
	x[1:]   = np.reshape(X[L], np.size(y)-1)

	F       = np.full(np.size(L), DUMMY_FLOAT, dtype=np.float64)
	F[L]    = copy.deepcopy(y[1:]) #F[L]    = y
	
	#[dF, d2F] = calc_derivative_mask(F, L, G, j_df, Type, jlev, order, h, der_order);[dF, d2F] = calc_derivative_mask(F, L, G, j_df, Type, jlev, order, h, der_order);
	dF, d2F = calc_derivative(F, L, Type, jlev, order, h, der_order)
	dy      = np.full(np.size(y), DUMMY_FLOAT, dtype=np.float64)
	d2y     = np.full(np.size(y), DUMMY_FLOAT, dtype=np.float64)
	dy[1:]  = np.reshape(dF[L],  np.size(y)-1, order="F").copy()
	d2y[1:] = np.reshape(d2F[L], np.size(y)-1, order="F").copy()

	return x, dy, d2y