# CALC_DERIVATIVE_MASK calculate derivatives on optimal grids.
#
# NOTE: 
# The derivatives must be calculated while the inverse transform is performed.
# Otherwise higher wavelets can pollute smoothness through lifting.
#
# F         is the function 
# L         is the mask where I have F given,
# G         is the derivative mask
# j_df      is index of levels of resolution used for derivative calculation
# Type      is either internal or periodic
# jlev      is the number of jleves
# order     is of the wavelet
# h         is mesh spacing
# der_order is order of derivative (1 or 2 only)
#
# Written In Matlab:    Oleg V. Vasilyev    |   4 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   20 November, 2023

import copy 
import numpy as np
from awcm1d.src.C_range_corr import C_range_corr as C_range_corr
from awcm1d.src.D_range_corr import D_range_corr as D_range_corr
from awcm1d.src.G_range_corr import G_range_corr as G_range_corr
from awcm1d.src.prd_corr import prd_corr as prd_corr
from awcm1d.src.wgh import wgh as wgh
from awcm1d.src.wtf import wtf as wtf
from awcm1d.src.df_pt import df_pt as df_pt
from awcm1d.src.d2f_pt import d2f_pt as d2f_pt
from awcm1d.src.constants import DUMMY_INT as DUMMY_INT
from awcm1d.src.constants import DUMMY_FLOAT as DUMMY_FLOAT

def calc_derivative_mask(F_in, L, G, j_df, Type, jlev, order, h, der_order):
    F = copy.deepcopy(F_in)

    #SIZE
    #CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
    ende    = np.size(L) - 1

    #MAKE SURE POINTS THAT ARE NOT NEEDED CAN BE ACCESSED (THEY SHOULD BE ZERO FOR ZERO CONTRIBUTION)
    F[~L]   = np.NaN   #ONLY UN-NANING

    #INITALIZE DERIVATIVE
    dF      = np.NaN*np.ones(np.size(L))
    d2F     = np.NaN*np.ones(np.size(L))

    Fn      = wtf(Type, F, jlev, order, 'forward', L);
    #MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
    Fn[~L]  = 0

    #CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
    n_range     = np.full(order+2, DUMMY_INT, dtype=int)
    n_range[1:] = np.arange(-np.fix((order+1)/2),  order-np.fix((order+1)/2)+1,     1, dtype=int) #[-fix((order+1)/2):order-fix((order+1)/2)];
    d_range     = np.full(2*int(np.fix((der_order+1)/2))+2, DUMMY_INT, dtype=int)
    d_range[1:] = np.arange(-np.fix((der_order+1)/2),    np.fix((der_order+1)/2)+1, 1, dtype=int) #[-fix((der_order+1)/2):fix((der_order+1)/2)];


    if Type.lower() == 'interval':
        
        #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        j_range = np.arange(1, jlev+1, 1, dtype=int) #1:jlev
        for j in j_range:
            s = 2**(jlev-j)
            G_ind = np.arange(1, ende+1, s, dtype=int) #[1:s:ende];
            G_ind = G_ind[np.nonzero(L[G_ind])]
            
            for i in G_ind[np.nonzero(j == j_df[G_ind])]:
                n_range_corr = G_range_corr(i, ende, s, d_range)
                Fn_dF = np.concatenate([[DUMMY_FLOAT], Fn[i+n_range_corr[1:]*s]])
                dF[i]  =  df_pt(Fn_dF, h*s, n_range_corr[1], n_range_corr[-1])
                d2F[i] = d2f_pt(Fn_dF, h*s, n_range_corr[1], n_range_corr[-1])

            if j < jlev:
                #STRIDE
                s = 2**(jlev-j-1)
                
                #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
                D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
                C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1:2*s:ende];
                
                D_ind = D_ind[np.nonzero(G[D_ind])]
                C_ind = C_ind[np.nonzero(G[C_ind])]
                
                #INVERSE UPDATE STAGE: REDISTRIBUTE WEIGHTS
                for i in C_ind:
                    n_range_corr = C_range_corr(i, ende, s, n_range)
                    order_corr = np.size(n_range_corr) - 2
                    k_range = np.arange(1, order_corr+2, 1, dtype=int) #1:order_corr+1
                    for k in k_range:
                        Fn[i] = Fn[i] - 0.5*Fn[i+(2*n_range_corr[k]+1)*s]*wgh(n_range_corr[k], n_range_corr[1], n_range_corr[order_corr+1])

                #INVERSE PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
                for i in D_ind:
                    n_range_corr = D_range_corr(i, ende, s, n_range)
                    order_corr = np.size(n_range_corr) - 2
                    k_range = np.arange(1, order_corr+2, 1, dtype=int) #1:order_corr+1
                    for k in k_range:
                        Fn[i] = Fn[i] + Fn[i+(2*n_range_corr[k]+1)*s]*wgh(n_range_corr[k], n_range_corr[1], n_range_corr[order_corr+1])
        
        #SET THE POINTS THAT WERE NOT DESIRED TO NaN -> THEY CANNOT BE USED
        Fn[~G]   = np.NaN
        
    elif Type.lower() == 'periodic':

        #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        j_range = np.arange(1, jlev+1, 1, dtype=int) #1:jlev
        for j in j_range:
            s = 2**(jlev-j)
            G_ind = np.arange(1, ende+1, s, dtype=int) #[1:s:ende];
            G_ind = G_ind[find(L[G_ind])]
            
            for i in G_ind[find(j == j_df[G_ind])]:
                dF[i]  =  df_pt(Fn[prd_corr(i+d_range*s, ende)], h*s, d_range[1], d_range[-1])
                d2F[i] = d2f_pt(Fn[prd_corr(i+d_range*s, ende)], h*s, d_range[1], d_range[-1])
            
            if j < jlev:
                #STRIDE
                s = 2**(jlev-j-1)
                
                #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
                D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
                C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1:2*s:ende];
                
                D_ind = D_ind[find(G[D_ind])]
                C_ind = C_ind[find(G[C_ind])]
                
                #INVERSE UPDATE STAGE: REDISTRIBUTE WEIGHTS
                k_range = np.arange(1, order+2, 1, dtype=int) #1:order+1
                for k in k_range:
                    Fn[C_ind] = Fn[C_ind] - 0.5*Fn[prd_corr(C_ind+(2*n_range[k]+1)*s, ende)]*wgh(n_range[k], n_range[1], n_range[order+1])
                
                #INVERSE PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
                for k in k_range:
                    Fn[D_ind] = Fn[D_ind] + Fn[prd_corr(D_ind+(2*n_range[k]+1)*s, ende)]*wgh(n_range[k], n_range[1], n_range[order+1])
        
        #SET THE POINTS THAT WERE NOT DESIRED TO NaN -> THEY CANNOT BE USED
        Fn[~G]   = np.NaN
    
    return dF, d2F
