# WTF a 1-D second generation wavelet transform
#
# Fn   - is the function or the transformed function
# jlev - number of jleves (clev can be calculated from that)
# L_in - in case Fn has been truncated than L_in knows how to translate Fn_trunc -> Fn, default logical(ones(size(Fn)))
# Flag - 'forward' or 'inverse', default 'forward'
#
# EXPLANATION OF D's AND C'S
#
#    1           2         3
#  D(3) �      D(2)      D(3)  �
#       � C(1)      C(2)      C(1)
#           1         2         3
#
# Written In Matlab:    Oleg V. Vasilyev    |   14 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   19 November, 2023

import copy 
import numpy as np
from awcm1d.src.C_range_corr import C_range_corr as C_range_corr
from awcm1d.src.D_range_corr import D_range_corr as D_range_corr
from awcm1d.src.prd_corr import prd_corr as prd_corr
from awcm1d.src.wgh import wgh as wgh
from awcm1d.src.constants import DUMMY_INT as DUMMY_INT

def wtf(Type, Fn_in, jlev, order=1, Flag='forward', L_in=None):
    Fn = copy.deepcopy(Fn_in)
    if L_in is None:
        L_in=np.ones(np.size(Fn), dtype=bool)

    if Type.lower() == 'interval':
        #CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
        ende    = np.size(L_in) - 1
        n_range     = np.full(order+2, DUMMY_INT, dtype=int)
        n_range[1:] = np.arange(-np.fix((order+1)/2), order-np.fix((order+1)/2)+1, 1, dtype=int) #[-fix((order+1)/2):order-fix((order+1)/2)]

        #==========================================================================================================================
        if Flag.lower() == 'forward':
            #MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
            Fn[~L_in] = 0. #Fn[~L_in[1:]] = 0
                   
            #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
            j_range = np.arange(jlev-1, 0, -1, dtype=int) #jlev-1:-1:1                        
            for j in j_range:
                #STRIDE
                s = 2**(jlev-j-1)
                
                #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
                D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
                C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1  :2*s:ende];
                #D_ind = np.linspace(1+s, ende, num=int(np.fix(abs(ende-1-s)/(2*s)))+1, endpoint=True, dtype=int) #[1+s:2*s:ende];
                #C_ind = np.linspace(1,   ende, num=int(np.fix(abs(ende-1)/(2*s)))+1,   endpoint=True, dtype=int) #[1  :2*s:ende];
                
                D_ind = D_ind[np.nonzero(L_in[D_ind])]
                C_ind = C_ind[np.nonzero(L_in[C_ind])]
                
                #PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
                for i in D_ind:
                    n_range_corr = D_range_corr(i, ende, s, n_range)
                    order_corr = np.size(n_range_corr) - 2
                    k_range = np.arange(1, order_corr+2, 1, dtype=int) #1:order_corr+1
                    for k in k_range:
                        Fn[i] = Fn[i] - Fn[i+(2*n_range_corr[k]+1)*s] * wgh(n_range_corr[k], n_range_corr[1], n_range_corr[order_corr+1])

                #UPDATE STAGE: REDISTRIBUTE WEIGHTS
                for i in C_ind:
                    n_range_corr = C_range_corr(i, ende, s, n_range)
                    order_corr = np.size(n_range_corr) - 2
                    k_range = np.arange(1, order_corr+2, 1, dtype=int) #1:order_corr+1
                    for k in k_range:
                        Fn[i] = Fn[i] + 0.5*Fn[i+(2*n_range_corr[k]+1)*s] * wgh(n_range_corr[k], n_range_corr[1], n_range_corr[order_corr+1])
            
        #==========================================================================================================================
        elif Flag.lower() == 'inverse':
            #MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
            Fn[~L_in] = 0.
            
            #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
            j_range = np.arange(jlev-1, 0, -1, dtype=int) #jlev-1:-1:1                        
            for j in j_range:
                #STRIDE
                s = 2**(jlev-j-1)
                
                #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
                D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
                C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1  :2*s:ende];
                
                D_ind = D_ind[np.nonzero(L_in[D_ind])]
                C_ind = C_ind[np.nonzero(L_in[C_ind])]
                
                #INVERSE UPDATE STAGE: REDISTRIBUTE WEIGHTS
                for i in C_ind:
                    n_range_corr = C_range_corr(i, ende, s, n_range)
                    order_corr = np.size(n_range_corr) - 2
                    k_range = np.arange(1, order_corr+2, 1, dtype=int) #1:order_corr+1
                    for k in k_range:
                        Fn[i] = Fn[i] - 0.5*Fn[i+(2*n_range_corr[k]+1)*s] * wgh(n_range_corr[k], n_range_corr[1], n_range_corr[order_corr+1])

                #INVERSE PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
                for i in D_ind:
                    n_range_corr = D_range_corr(i, ende, s, n_range)
                    order_corr = np.size(n_range_corr) - 2
                    k_range = np.arange(1, order_corr+2, 1, dtype=int) #1:order_corr+1
                    for k in k_range:
                        Fn[i] = Fn[i] + Fn[i+(2*n_range_corr[k]+1)*s] * wgh(n_range_corr[k], n_range_corr[1], n_range_corr[order_corr+1])
            
            #SET THE POINTS THAT WERE NOT DESIRED TO NaN -> THEY CANNOT BE USED
            Fn[~L_in] = np.NaN

        
    elif Type.lower() == 'periodic':
        #CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
        ende    = np.size(L_in) - 1
        n_range     = np.full(order+2, DUMMY_INT, dtype=int)
        n_range[1:] = np.arange(-np.fix((order+1)/2), order-np.fix((order+1)/2)+1, 1, dtype=int) #[-fix((order+1)/2):order-fix((order+1)/2)]
        
        #==========================================================================================================================
        if Flag.lower() == 'forward':
            #MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
            Fn[~L_in] = 0.
            
            #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
            j_range = np.arange(jlev-1, 0, -1, dtype=int) #jlev-1:-1:1                        
            for j in j_range:
                #STRIDE
                s = 2**(jlev-j-1)
                
                #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
                D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
                C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1  :2*s:ende];
                
                D_ind = D_ind[np.nonzero(L_in[D_ind])]
                C_ind = C_ind[np.nonzero(L_in[C_ind])]
                
                #PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
                k_range = np.arange(1, order+2, 1, dtype=int) #1:order+1
                for k in k_range:
                    Fn[D_ind] = Fn[D_ind] - Fn[prd_corr(D_ind+(2*n_range[k]+1)*s,ende)] * wgh(n_range[k], n_range[1], n_range[order+1])

                #UPDATE STAGE: REDISTRIBUTE WEIGHTS
                for k in k_range:
                    Fn[C_ind] = Fn[C_ind] + 0.5*Fn[prd_corr(C_ind+(2*n_range[k]+1)*s,ende)] * wgh(n_range[k], n_range[1], n_range[order+1])
            
            #REDUCE RESOLUTION ACCORDING TO ACCURACY CRITERIUM
            Fn[~L_in] = np.NaN
            
        #==========================================================================================================================
        elif Flag.lower() == 'inverse':
            #MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
            Fn[~L_in] = 0.
         
            j_range = np.arange(1, jlev, 1, dtype=int) #1:jlev-1                        
            for j in j_range:                                                   #LOOP THROUGH THE LEVELS
                s = 2**(jlev-j-1)
                
                #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION 
                D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
                C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1  :2*s:ende];
                
                D_ind = D_ind[np.nonzero(L_in[D_ind])]
                C_ind = C_ind[np.nonzero(L_in[C_ind])]
                
                #INVERSE UPDATE STAGE: REDISTRIBUTE WEIGHTS
                k_range = np.arange(1, order+2, 1, dtype=int) #1:order+1
                for k in k_range:
                    Fn[C_ind] = Fn[C_ind] - 0.5*Fn[prd_corr(C_ind+(2*n_range[k]+1)*s,ende)] * wgh(n_range[k], n_range[1], n_range[order+1])

                #INVERSE PREDICT STAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
                for k in k_range:
                    Fn[D_ind] = Fn[D_ind] + Fn[prd_corr(D_ind+(2*n_range[k]+1)*s,ende)] * wgh(n_range[k], n_range[1], n_range[order+1])
            
            #SET THE POINTS THAT WERE NOT DESIRED TO NaN -> THEY CANNOT BE USED
            Fn[~L_in] = np.NaN

    return Fn