# G         is the mask for points for derivative calculation
# j_df      is the array of levels at which the derivative is calculated
# L         is the mask where F is given,
# Type      is either internal or periodic
# jlev      is the number of jleves
# der_order is polynomial order of derivative approximation
#
# Written In Matlab:    Oleg V. Vasilyev    |   14 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   20 November, 2023

import numpy as np
import copy
from awcm1d.src.G_range_corr import G_range_corr as G_range_corr
from awcm1d.src.prd_corr import prd_corr as prd_corr
from awcm1d.src.constants import DUMMY_INT as DUMMY_INT

def add_ghost(L, Type, jlev, der_order):
    ende = np.size(L) - 1

    #Calculates derivative level
    j_df = np.ones(np.size(L), dtype=int)
    j_df[~L] = 0

    n_range     = np.full(3, DUMMY_INT, dtype=int)
    n_range[1:] = np.array([-1, 0]) # just near neighbors to calculate derivatieve level

    j_range = np.arange(1, jlev, 1, dtype=int) #1:jlev-1
    for j in j_range:
        s = 2**(jlev-j-1)

        D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
        D_ind = D_ind[np.nonzero(L[D_ind])]
        j_df[D_ind] = j+1

    if Type.lower() == 'interval':        
        j_range = np.arange(1, jlev, 1, dtype=int) #1:jlev-1
        for j in j_range:
            s = 2**(jlev-j-1);
            
            D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
            C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1:2*s:ende];
            D_ind = D_ind[np.nonzero(L[D_ind])]
            C_ind = C_ind[np.nonzero(L[C_ind])]
            
            for i in D_ind:
                n_range_corr = G_range_corr(i, ende, s, n_range)
                k_range = np.arange(1, np.size(n_range_corr), 1, dtype=int) #1:length(n_range_corr)
                for k in k_range:
                    j_df[i+(2*n_range_corr[k]+1)*s] = j+1

    elif Type.lower() == 'periodic':        
        j_range = np.arange(1, jlev, 1, dtype=int) #1:jlev-1
        for j in j_range:                                                  
            s = 2**(jlev-j-1)
            
            D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
            C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1:2*s:ende];
            D_ind = D_ind[np.nonzero(L[D_ind])]
            C_ind = C_ind[np.nonzero(L[C_ind])]
            
            k_range = np.arange(1, np.size(n_range), 1, dtype=int) #1:length(n_range)
            for k in k_range:
                j_df[prd_corr(D_ind+(2*n_range[k]+1)*s,ende)] = j+1


    #Add ghost points for derivative calculation
    G = copy.deepcopy(L)
    n_range     = np.full(2*int(np.fix((der_order+1)/2))+2, DUMMY_INT, dtype=int)
    n_range[1:] = np.arange(-np.fix((der_order+1)/2), np.fix((der_order+1)/2)+1, 1, dtype=int) #[-fix((der_order+1)/2):fix((der_order+1)/2)];
    
    if Type.lower() == 'interval': 
        j_range = np.arange(1, jlev+1, 1, dtype=int) #1:jlev
        for j in j_range:
            s = 2**(jlev-j)
            
            G_ind = np.arange(1, ende+1, s, dtype=int) #[1:s:ende];
            G_ind = G_ind[np.nonzero(L[G_ind])]
            
            for i in G_ind[np.nonzero(j == j_df[G_ind])]:
                n_range_corr = G_range_corr(i, ende, s, n_range)
                k_range = np.arange(1, np.size(n_range_corr), 1, dtype=int) #1:length(n_range_corr)
                for k in k_range: 
                    G[i+n_range_corr[k]*s] = True

    elif Type.lower() == 'periodic':  
        j_range = np.arange(1, jlev+1, 1, dtype=int) #1:jlev      
        for j in j_range:
            s = 2**(jlev-j)
            
            G_ind = np.arange(1, ende+1, s, dtype=int) #[1:s:ende];
            G_ind = G_ind[np.nonzero(L[G_ind])]
            G_ind = G_ind[np.nonzero(j == j_df[G_ind])]

            k_range = np.arange(1, np.size(n_range), 1, dtype=int) #1:length(n_range)
            for k in k_range: 
                G[prd_corr(G_ind+n_range[k]*s, ende)] = True
        
    return G, j_df

