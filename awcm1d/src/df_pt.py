# DF_PT Calculates first derivative of a funciton at a point X_s for uniformly spaced points X_k= k*Delta  
# (k=(s+n_l)...(s+n_h))
#
# f  - array of function values of the size (N=n_h-n_l+1)
# nl - How many point do I want to use to the left  (could be to the right)
# nh - How many point do I want to use to the right (could be to the right)
#
# EXAMPLE
# diff_pt(f(1:3), 0.1, -1, 1) - Calculates deirvative using using central difference assuming 
# the spacing is 0.1
#
# Written In Matlab:    Oleg V. Vasilyev    |   4 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   19 November, 2023

import numpy as np
from awcm1d.src.dwgh import dwgh as dwgh

def df_pt(f, h, n_l, n_h):
    df = 0.0
    j_range = np.arange(n_l, n_h+1, 1, dtype=int) #[n_l:n_h]
    for j in j_range:
        df = df + f[j-n_l+1]*dwgh(j,n_l,n_h,h)

    return df