# WTF a 1-d second generation wavelet transform
#
# Fn   - is the function or the transformed function
# L_in - mask
# scl  - absolute scale for adaptation
# acc  - relative scale for adaptation
# jlev - number of jleves
# Jmx  - maximum allowable level of resolution
#
# Written In Matlab:    Oleg V. Vasilyev    |   11 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   20 November, 2023

import numpy as np

def significant(Fn, L_in, scl, acc, jlev):
    ende = np.size(Fn) - 1
    Fn[~L_in] = 0.

    #FORWARD WAVELET TRANSFORM - GO FROM THE FINEST (jlev-1) TO THE COARSEST LEVEL
    #Mask of significan wavelets - initialize to false.
    L_sig = np.zeros(np.size(L_in), dtype=bool) #Matlab logical( zeros(size(L_in)));      

    #CHECK SIGNIFICANT WAVELETS
    j_range = np.arange(jlev-1, 0, -1, dtype=int) #jlev-1:-1:1
    for j in j_range:
        #STRIDE
        s = 2**(jlev-j-1)
         
        #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
        D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
        C_ind = np.arange(1,   ende+1, 2*s, dtype=int) #[1:2*s:ende];
         
        D_ind = D_ind[np.nonzero(L_in[D_ind])]
        C_ind = C_ind[np.nonzero(L_in[C_ind])]
         
        #IF THE DIFFERENCE BETWEEN INTERPOLATED SIGNAL AND ORIGINAL SIGNAL WAS BIGGER THAN ACCURACY -> STORE POINT
        L_sig[D_ind] = (abs(Fn[D_ind])>=acc*scl) | L_sig[D_ind]
         
        #FALG COURSEST LEVEL TRUE
        if j == 1:
            L_sig[C_ind] = True
        
    return L_sig
