# RECONSTRUCTION CHECK if all the supporting wavelets are present and does the correction if necessary.
# This must be done because maybe through the adding of neighbours some wavelets are placed without support.
#
# Type - 'internal' or 'periodic'
# jlev - number of jleves (clev can be calculated from that)
# L_in - mask of significan wavelet
# order - order of the wavelet 
#
# Written In Matlab:    Oleg V. Vasilyev    |   4 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   20 November, 2023

import numpy as np
from awcm1d.src.D_range_corr import D_range_corr as D_range_corr
from awcm1d.src.prd_corr import prd_corr as prd_corr
from awcm1d.src.constants import DUMMY_INT as DUMMY_INT

def reconstruction_check(Type, jlev, order, L_in):
    ende       = np.size(L_in) - 1
    L_out      = L_in
          
    if Type.lower() == 'interval':
        #CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
        n_range     = np.full(order+2, DUMMY_INT, dtype=int)
        n_range[1:] = np.arange(-np.fix((order+1)/2), order-np.fix((order+1)/2)+1, 1, dtype=int) #[-fix((order+1)/2):order-fix((order+1)/2)];

        #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        j_range = np.arange(jlev-1, 0, -1, dtype=int) #jlev-1:-1:1
        for j in j_range:
            #STRIDE
            s = 2**(jlev-j-1)
            
            #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
            D_ind = D_ind[np.nonzero(L_out[D_ind])]
            
            #RECONSTRUCTION CHECK: IF ANY D IS FLAGGED THEN IT'S NEIGBOURS ON THE LEVEL, REQUIRED FOR PREDICT STAGE, MUST BE FLAGGED
            for i in D_ind:
                n_range_corr = D_range_corr(i, ende, s, n_range)
                order_corr = np.size(n_range_corr) - 2
                k_range = np.arange(1, order_corr+2, 1, dtype=int) #1:order_corr+1
                for k in k_range:
                    L_out[i+(2*n_range_corr[k]+1)*s] = L_out[i+(2*n_range_corr[k]+1)*s] | L_out[i]
        
    elif Type.lower() == 'periodic':
        #CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
        n_range     = np.full(order+2, DUMMY_INT, dtype=int)
        n_range[1:] = np.arange(-np.fix((order+1)/2),  order-np.fix((order+1)/2)+1,   1, dtype=int) #[-fix((order+1)/2):order-fix((order+1)/2)];
        
        #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        j_range = np.arange(jlev-1, 0, -1, dtype=int) #jlev-1:-1:1
        for j in j_range:
            #STRIDE
            s = 2**(jlev-j-1)
            
            #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            D_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
            D_ind = D_ind[np.nonzero(L_out[D_ind])]
            
            #REDCONSTRUCTION CHECK: IF ANY D IS FLAGGED THEN IT'S NEIGBOURS ON THE LEVEL, REQUIRED FOR PREDICT STAGE, MUST BE FLAGGED
            k_range = np.arange(1, order+2, 1, dtype=int) #1:order+1
            for k in k_range:
                L_out[prd_corr(D_ind+(2*n_range[k]+1)*s, ende)] = L_out[prd_corr(D_ind+(2*n_range[k]+1)*s, ende)] | L_out[D_ind]
            
    return L_out