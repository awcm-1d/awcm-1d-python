# D2WGH calculates weights for the second derivative calculation for 
# uniformly spaced Lagrange interpolation at X_s point using X_l= l*Delta points (l=(s+n_l)...(s+n_h))
#
# h  - mesh spacing
# k  - Index of the point
# nl - How many point do I want to use to the left  (could be to the right)
# nh - How many point do I want to use to the right (could be to the right)
#
# EXAMPLE
# d2wgh( 0,  0, 1, 0.1) - Calculates the weight in point  0 using assymetric
# stencil with 0 points on the left and 1 point on the right with 0.1 spacing
# d2wgh(-1, -2, 1, 0.1) - Calculates the weight in point  0 using assymetric
# stencil with 2 points on the left and 1 point on the right with 0.1 spacing
#
# Written In Matlab:    Oleg V. Vasilyev    |   4 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   19 November, 2023

import numpy as np

def d2wgh(k, n_l, n_h,h):
    d2wgh = 0
    i_range = np.arange(n_l, n_h+1, 1, dtype=int) #[n_l:n_h];
    i = i_range[np.nonzero(i_range != k)]
    for m in i:
        j = i[np.nonzero(i != m)]
        for n in j:
            d2wgh = d2wgh + np.prod(j[np.nonzero(j != n)])
    d2wgh = d2wgh / np.prod(i-k) / h**2

    return d2wgh


