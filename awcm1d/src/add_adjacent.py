# ADD_ADJACENT adds the neighbours needed to catch where the energy goes to.
# 
# Neighbours are added on the same level and one level above.
#
# Type - 'internal' or 'periodic'
# L_in - mask of significan wavelet
# jlev - number of jleves (clev can be calculated from that)
#
# Written In Matlab:    Oleg V. Vasilyev    |   14 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   20 November, 2023

import numpy as np
import copy
from awcm1d.src.prd_corr import prd_corr as prd_corr

def add_adjacent(Type, L_in, jlev):

    ende  = np.size(L_in) - 1
    L_in[0] = False  # Just to reassure
    L_adj = L_in
          
    if Type.lower() == 'interval':
        #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        j_range = np.arange(jlev, 0, -1, dtype=int) #jlev:-1:1
        for j in j_range:
            s = 2**(jlev-j)
            
            #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            if j == 1:
                S_ind = np.arange(1,   ende+1,   s, dtype=int) #[1:s:ende];
            else:
                S_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende]; 
            S_ind = S_ind[np.nonzero(L_in[S_ind])]
            
            #RECONSTRUCTION CHECK: IF ANY D IS FLAGGED THEN IT'S NEIGBOURS ON THE LEVEL, REQUIRED FOR PREDICT STAGE, MUST BE FLAGGED       
            if np.size(S_ind) > 0:
                i_range = np.array([-1,1]) * int(s/2**(min(j+1,jlev)-j))
                for i in i_range:
                    L_adj[S_ind[(S_ind + i >=1) & (S_ind + i <= ende)] + i] = True

    elif Type.lower() == 'periodic':
        #LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        j_range = np.arange(jlev, 0, -1, dtype=int) #jlev:-1:1
        for j in j_range:
            s = 2**(jlev-j)
            
            #SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            if j == 1:
                S_ind = np.arange(1,   ende+1,   s, dtype=int) #[1:s:ende];
            else:
                S_ind = np.arange(1+s, ende+1, 2*s, dtype=int) #[1+s:2*s:ende];
            S_ind = S_ind[np.nonzero(L_in[S_ind])]
            
            #RECONSTRUCTION CHECK: IF ANY D IS FLAGGED THEN IT'S NEIGBOURS ON THE LEVEL, REQUIRED FOR PREDICT STAGE, MUST BE FLAGGED       
            if np.size(S_ind) > 0:
                i_range = np.array([-1,1]) * int(s/2**(min(j+1,jlev)-j))
                for i in i_range:
                    L_adj[prd_corr(S_ind + i,ende)] = True

    return L_adj
        