# WGH calculates weights for uniformly spaced Lagrange interpolation to the half
# point X_{s-1/2} using X_l= l*Delta points (l=(s+n_l)...(s+n_h))
#
# k  - Index of the point
# nl - How many point do I want to use to the left  (could be to the right)
# nh - How many point do I want to use to the right (could be to the right)
#
# EXAMPLE
# wgh(0,   0, 1) - Calculates the weight in point  0 using first order extrapolation to point -1/2 from points 0,   1
# wgh(-1, -2, 1) - Calculates the weight in point -1 using third order intrapolation to point -1/2 from points -2, -1, 0, 1
#
# Written In Matlab:    Oleg V. Vasilyev    |   4 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   18 November, 2023

import numpy as np

def wgh(k, n_l, n_h):
	i_range = np.arange(n_l, n_h+1, 1, dtype=int) #[n_l:n_h];
	i = i_range[np.nonzero(i_range != k)]
	wgh = np.prod(i+1/2) / np.prod(i-k)

	return wgh