# D_RANGE_CORR the bound to ensure the points do not go outside the interval
#
# Written In Matlab:    Oleg V. Vasilyev    |   11 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   19 November, 2023

import numpy as np

def D_range_corr(Ind, n, s, N_range):
	N_range_corr = Ind + s * (2*N_range[1:] + 1)
	N_range_corr = N_range_corr + max(1,N_range_corr[0]) - N_range_corr[0]
	N_range_corr = N_range_corr + min(n,N_range_corr[-1])- N_range_corr[-1]
	N_range_corr = ((N_range_corr[np.nonzero(N_range_corr > 0)] - Ind)/s -1)/2
	N_range_corr = N_range_corr.astype(int)

	return np.concatenate([[N_range[0]], N_range_corr])