# PRD_CORR wraps the points around
#
# Written In Matlab:    Oleg V. Vasilyev
# Converted to Python:  Ari Nejadmalayeri   |   20 November, 2023

import numpy as np

def prd_corr(Ind, n):
	Ind = np.fmod(Ind-1+n, n)+1

	return Ind

