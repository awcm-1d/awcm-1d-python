# ADAPT_GRID using global definitions
#
# Fn         is the function 
#
# Written In Matlab:    Oleg V. Vasilyev    |   14 October, 2018
# Converted to Python:  Ari Nejadmalayeri   |   19 November, 2023

import copy 
import numpy as np
from awcm1d.src.wtf import wtf as wtf
from awcm1d.src.significant import significant as significant
from awcm1d.src.adjust_lev import adjust_lev as adjust_lev
from awcm1d.src.add_adjacent import add_adjacent as add_adjacent
from awcm1d.src.reconstruction_check import reconstruction_check as reconstruction_check
from awcm1d.src.add_ghost import add_ghost as add_ghost
from awcm1d.src.calc_derivative_mask import calc_derivative_mask as calc_derivative_mask
from awcm1d.src.constants import DUMMY_FLOAT as DUMMY_FLOAT

def adapt_grid(Fn_in, L_in, Type, jlev, Jmx, scl, acc, order, hh, XX):
	# global ende G j_df Type jlev Jmx scl acc order h hh XX x
	Fn = copy.deepcopy(Fn_in)

	Fn   = wtf(Type, Fn, jlev, order, 'forward', L_in)
	Fn[~L_in] = 0.0

	L_sig = significant(Fn, L_in, scl, acc, jlev)

	Fn, L_sig, jlev, ende = adjust_lev(Fn, L_sig, jlev, Jmx)

	L_sig = add_adjacent(Type, L_sig, jlev)

	L_out = reconstruction_check(Type, jlev, order, L_sig)

	G, j_df = add_ghost(L_out, Type, jlev, order)

	s     = 2**(Jmx-jlev)
	h     = hh*s                           #h - mesh spacing on the finest level
	X     = np.full(int((np.size(XX)-1)/s)+1, DUMMY_FLOAT, dtype=np.float64)    
	X[1:] = XX[1::s]                       #X - grid on finest level

	# L_out = L
	F_out = wtf(Type, Fn, jlev, order, 'inverse', L_out)
	F_out[~L_out] = np.NaN

	return F_out, L_out, G, j_df, X, s, h, jlev, ende